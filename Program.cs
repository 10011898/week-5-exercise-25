﻿using System;

namespace week_5_exercise_25
{
    public class Person
    {
        private string Name;
        private int Age;
        public Person(string _name, int _age)
        {
            Name = _name;
            Age = _age;
        }
        public void SayHello()
        {
            Console.WriteLine($"Hello, {Name}! You are {Age} years old.");
        }
    }

    public class Checkout
    {
        private int Quantity;
        private double Price;
        public Checkout(double _price, int _quantity)
        {
            Quantity = _quantity;
            Price = _price;
        }
        public double CheckOut()
        {
            return Quantity*Price;
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            var joe = new Person("Joe",29);
            joe.SayHello();
            Console.WriteLine();

            var itemOne   = new Checkout(5.99,10);
            var itemTwo   = new Checkout(7.45,8);
            var itemThree = new Checkout(4.89,3);

            var a1 = itemOne.CheckOut();
            var a2 = itemTwo.CheckOut();
            var a3 = itemThree.CheckOut();

            Console.WriteLine(a1 + a2 + a3);

        }
    }
}